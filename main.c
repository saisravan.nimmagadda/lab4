
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>
#include <memory.h>

static const long Num_To_Sort = 1000000L;
// quick sort logic taken from book "introduction to algorithms by robert sedgewick" https://www.geeksforgeeks.org/
void quickSortSequential(int a[], int low, int high)
{
    int i, j,mid,pivotal,temp;
    if(low >= high)
        return;
    // taking middle element as pivotal
    mid=(low+high)/2;
    pivotal=a[mid];
    // hiding pivotal by swapping with last element
    temp=a[mid];
    a[mid]=a[high];
    a[high]=temp;
    // setting i&j pointers to beginning and ending of array
    i=low;
    j=high-1;

    while (1)
    {
        //selecting big in left side of array
        while(i <= j && a[i] <= pivotal) i++;

        //selecting small in right side of array
        while(i<j && a[j] > pivotal) j--;

        if(i<j)
        {   // exchanging every pair of selection
            temp=a[i];
            a[i]=a[j];
            a[j]=temp;
        }
        else  // stop when i&j crossed each other
             break;
    }
    // restoring pivotal
    temp=a[i];
    a[i]=a[high];
    a[high]=temp;

    // sorting left partion recursively
    quickSortSequential(a, low,i-1);

    // sorting right partion recursively
    quickSortSequential(a, i+1,high);
 }

// quick sort logic taken from book "introduction to algorithms by robert sedgewick" https://www.geeksforgeeks.org/
void quickSortParallel(int a[], int low, int high)
{
    int i, j,mid,pivotal,temp;
    if (low >= high)
        return;
    // taking middle element as pivotal
    mid=(low+high)/2;
    pivotal=a[mid];
    // hiding pivotal by swapping with last element
    temp=a[mid];
    a[mid]=a[high];
    a[high]=temp;
    // setting i&j pointers to beginning and ending of array
    i=low;
    j=high-1;
    while(1)
    {
        //selecting big in left side of array
        while(i <= j && a[i] <= pivotal) i++;

        //selecting small in right side of array
        while(i<j && a[j] > pivotal) j--;

        if(i<j)
        {   // exchanging every pair of selection
            temp=a[i];
            a[i]=a[j];
            a[j]=temp;
        }
        else  // stop when i&j crossed each other
             break;
    }
    // restoring pivotal
    temp=a[i];
    a[i]=a[high];
    a[high]=temp;
   
   
     #pragma omp Parallel
     {
         // sorting left partion recursively
         quickSortParallel(a, low,i-1);

         // sorting right partion recursively
         quickSortParallel(a, i+1,high);
     
     }
 }


// Sequential version of your sort
void sort_s(int *arr)
{
     quickSortSequential(arr,0,Num_To_Sort-1);
     
}

// Parallel version of your sort
void sort_p(int *arr)
{
    quickSortParallel(arr,0,Num_To_Sort-1);
}

int main()
{
    int *arr_s = malloc(sizeof(int) * Num_To_Sort);
    long chunk_size = Num_To_Sort / omp_get_max_threads();
    #pragma omp parallel num_threads(omp_get_max_threads())
    {
        int p = omp_get_thread_num();
        long i;
        unsigned int seed = (unsigned int) time(NULL) + (unsigned int) p;
        long chunk_start = p * chunk_size;
        long chunk_end = chunk_start + chunk_size;
        for(i = chunk_start; i < chunk_end; i++)
        {
            arr_s[i] = rand_r(&seed);
        }
    }

    // Copy the array so that the sorting function can operate on it directly.
    // Note that this doubles the memory usage.
    // You may wish to test with slightly smaller arrays if you're running out of memory.
    int *arr_p = malloc(sizeof(int) * Num_To_Sort);
    memcpy(arr_p, arr_s, sizeof(int) * Num_To_Sort);
    struct timeval start, end;

    printf("Timing sequential...\n");
    gettimeofday(&start, NULL);
    sort_s(arr_s);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    free(arr_s);

    printf("Timing parallel...\n");
    gettimeofday(&start, NULL);
    sort_p(arr_p);
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    free(arr_p);
    return 0;
}


